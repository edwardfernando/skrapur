package com.seagatesoft.skrapur.app;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.seagatesoft.skrapur.Config;
import com.seagatesoft.skrapur.SearchProvider;
import com.seagatesoft.skrapur.db.DatabaseConnector;
import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;
import com.seagatesoft.skrapur.model.Station;

public class PopularRoutesChecker {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final SearchProvider searchProvider = new SearchProvider();
	private final DatabaseConnector dbConnector = DatabaseConnector.getInstance();
	private final Config config = Config.getInstance();
	private final int maxBookingDays = Integer.parseInt(config.getProperty("max_booking_days"));
	private String[] popularRoutes;
	private DateTime firstDepartureDate = DateTime.now();

	public String[] getPopularRoutes() {
		return popularRoutes;
	}

	public void setPopularRoutes(String[] popularRoutes) {
		this.popularRoutes = popularRoutes;
	}

	public DateTime getFirstDepartureDate() {
		return firstDepartureDate;
	}

	public void setFirstDepartureDate(DateTime firstDepartureDate) {
		this.firstDepartureDate = firstDepartureDate;
	}

	public void setPopularRoutesFromDb() {
		setPopularRoutes(dbConnector.getPopularRoutes());
	}

	private List<SearchRequest> createSearchRequests() {
		List<SearchRequest> searchRequests = Lists.newArrayList();
		DateTime departureDate = firstDepartureDate;
		DateTime lastBookableDate = departureDate.plusDays(maxBookingDays);

		while (!departureDate.isAfter(lastBookableDate)) {
			for (String popularRoute : popularRoutes) {
				String[] route = StringUtils.split(popularRoute, '-');
				Station origin = new Station();
				origin.setCode(route[0]);
				Station destination = new Station();
				destination.setCode(route[1]);

				SearchRequest searchRequest = new SearchRequest();
				searchRequest.setDepartureDate(departureDate);
				searchRequest.setOrigin(origin);
				searchRequest.setDestination(destination);
				searchRequest.setAdultPassengers(1);
				searchRequests.add(searchRequest);
			}

			departureDate = departureDate.plusDays(1);
		}

		return searchRequests;
	}

	public void checkFaresAsynchronous() {
		dbConnector.expireSearchResults();
		searchProvider.bulkSearchAndPost(createSearchRequests());
		searchProvider.closeAsyncHttpClient();
		dbConnector.destroy();
	}

	public void checkFaresSynchronous() {
		dbConnector.expireSearchResults();
		List<SearchRequest> searchRequests = createSearchRequests();

		long beginTime = System.currentTimeMillis();

		if (logger.isInfoEnabled()) {
			logger.info("Start searching for {} requests.", searchRequests.size());
		}

		int counter = 0;
		for (SearchRequest searchRequest : searchRequests) {
			try {
				long startTime = System.currentTimeMillis();
				searchRequest.setId(Integer.toString(counter));
				List<SearchResult> searchResults = searchProvider.search(searchRequest);
				long searchingTime = System.currentTimeMillis();
				dbConnector.saveSearchResults(searchRequest, searchResults);
				long postingTime = System.currentTimeMillis();

				if (logger.isInfoEnabled()) {
					logger
					    .info(
					        "Finished searching and posting for {} with {} results. Search time: {}, posting time: {}",
					        searchRequest,
					        searchResults.size(),
					        searchingTime - startTime,
					        postingTime - searchingTime);
				}
			} catch (Exception e) {
				logger.error(
				    "Error when searching for search requests: {}. Message: {}",
				    searchRequest,
				    e.getMessage());

				if (logger.isInfoEnabled()) {
					e.printStackTrace();
				}
			} finally {
				counter++;
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("Finished searching in {} ms", (System.currentTimeMillis() - beginTime));
		}

		searchProvider.closeAsyncHttpClient();
		dbConnector.destroy();
	}

	public static void main(String[] args) {
		PopularRoutesChecker checker = new PopularRoutesChecker();
		checker.setPopularRoutesFromDb();
		checker.checkFaresSynchronous();
	}
}
