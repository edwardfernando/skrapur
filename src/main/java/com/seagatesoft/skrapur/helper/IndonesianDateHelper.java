package com.seagatesoft.skrapur.helper;

import java.util.List;

import com.google.common.collect.Lists;

public class IndonesianDateHelper {
	private static final List<String> MONTH_NAMES = Lists.newArrayList();
	private static final List<String> DAY_NAMES = Lists.newArrayList();
	static {
		MONTH_NAMES.add("Januari");
		MONTH_NAMES.add("Februari");
		MONTH_NAMES.add("Maret");
		MONTH_NAMES.add("April");
		MONTH_NAMES.add("Mei");
		MONTH_NAMES.add("Juni");
		MONTH_NAMES.add("Juli");
		MONTH_NAMES.add("Agustus");
		MONTH_NAMES.add("September");
		MONTH_NAMES.add("Oktober");
		MONTH_NAMES.add("November");
		MONTH_NAMES.add("Desember");

		DAY_NAMES.add("Senin");
		DAY_NAMES.add("Selasa");
		DAY_NAMES.add("Rabu");
		DAY_NAMES.add("Kamis");
		DAY_NAMES.add("Jumat");
		DAY_NAMES.add("Sabtu");
		DAY_NAMES.add("Minggu");
	}

	public static String getMonthName(int monthIndex) {
		if (monthIndex >= 1 && monthIndex <= 12) {
			return MONTH_NAMES.get(monthIndex - 1);
		} else {
			return null;
		}
	}

	public static String getDayName(int dayIndex) {
		if (dayIndex >= 1 && dayIndex <= 7) {
			return DAY_NAMES.get(dayIndex - 1);
		} else {
			return null;
		}
	}
}
