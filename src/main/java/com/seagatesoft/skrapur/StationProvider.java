package com.seagatesoft.skrapur;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;
import com.seagatesoft.skrapur.helper.FileHelper;
import com.seagatesoft.skrapur.model.Station;

public class StationProvider {
	private static final StationProvider stationProvider = new StationProvider();
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Map<String, Station> STATIONS_MAP = Maps.newHashMap();

	private StationProvider() {
		super();
		initializeStationsMap();
	}

	public static StationProvider getInstance() {
		return stationProvider;
	}

	private void initializeStationsMap() {
		Config config = Config.getInstance();
		String csvFile = config.getProperty("stations_list_file");
		BufferedReader br = null;
		String line = "";
		char csvSplitBy = ',';

		try {
			br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/"+csvFile)));

			while ((line = br.readLine()) != null) {
				String[] stationInfo = StringUtils.split(line, csvSplitBy);
				Station station = new Station();
				station.setCode(stationInfo[0]);
				station.setName(stationInfo[1]);
				station.setCity(stationInfo[2]);
				STATIONS_MAP.put(station.getCode(), station);
			}
		} catch (FileNotFoundException e) {
			logger.error("stations.csv not found: {}", e.getMessage());
		} catch (IOException e) {
			logger.error("IOException when reading the file: {}",
					e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.error("IOException when closing the buffer: {}",
							e.getMessage());
				}
			}
		}
	}

	public List<Station> scrapeStationList() throws InterruptedException,
			ExecutionException, IOException {
		List<Station> stations = Lists.newArrayList();

		AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
		Request searchFormRequest = new RequestBuilder().setMethod("GET")
				.setUrl("https://tiket.kereta-api.co.id/").build();
		long startTime = System.currentTimeMillis();
		Response searchFormResponse = asyncHttpClient
				.prepareRequest(searchFormRequest).execute().get();
		long duration = System.currentTimeMillis() - startTime;

		if (logger.isInfoEnabled()) {
			logger.info("Response time = {} ms", duration);
		}

		asyncHttpClient.close();
		startTime = System.currentTimeMillis();
		Document searchForm = Jsoup.parse(searchFormResponse.getResponseBody());
		Elements cityElements = searchForm
				.select("select[name=origination] > optgroup");

		if (logger.isDebugEnabled()) {
			FileHelper.writeToFile("search_form.html",
					searchFormResponse.getResponseBody());
		}

		for (Element cityElement : cityElements) {
			String city = cityElement.attr("label");
			Elements stationElements = cityElement.select("option");

			for (Element stationElement : stationElements) {
				String[] stationInfo = StringUtils.split(stationElement.val(),
						'#');
				Station station = new Station();
				station.setCode(stationInfo[0]);
				station.setName(stationInfo[1]);
				station.setCity(city);
				stations.add(station);
			}
		}

		duration = System.currentTimeMillis() - startTime;

		if (logger.isInfoEnabled()) {
			logger.info("Parsing time = {} ms", duration);
		}

		return stations;
	}

	public void writeStationListToFile(String fileName)
			throws InterruptedException, ExecutionException, IOException {
		List<Station> stations = scrapeStationList();
		File file = new File(fileName);

		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		for (Station station : stations) {
			bw.append(station.getCode()).append(',').append(station.getName())
					.append(',').append(station.getCity());
			bw.newLine();
		}

		bw.close();
	}

	public Map<String, Station> getStationList() {
		return STATIONS_MAP;
	}

	public Station getStation(String stationCode) {
		return STATIONS_MAP.get(stationCode);
	}

	public boolean isStationCodeExists(String stationCode) {
		return STATIONS_MAP.containsKey(stationCode);
	}
}
