package com.seagatesoft.skrapur;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.seagatesoft.skrapur.model.SearchResult;

public class SearchParser {
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat
			.forPattern("yyyyMMddHHmm");
	private static final DateTimeFormatter ALTERNATE_DATE_TIME_FORMATTER = DateTimeFormat
			.forPattern("HH:mm dd MMM yyyy");
	private static final Pattern TRAIN_INFO_PATTERN = Pattern
			.compile("([A-Z ]{3,}) - Nomor KA : (\\d+)");
	private static final Pattern CABIN_INFO_PATTERN = Pattern
			.compile("(\\w{3}) \\(([A-Z])\\)");
	private static final String TICKET_AVAILABLE_STATUS = "Tersedia";
	private static final Map<String, String> CABIN_NAMES_MAP = Maps
			.newHashMap();
	static {
		CABIN_NAMES_MAP.put("Eko", "K");
		CABIN_NAMES_MAP.put("Bis", "B");
		CABIN_NAMES_MAP.put("Eks", "E");
	}

	public List<SearchResult> parse(String docString) {
		Document doc = Jsoup.parse(docString);
		Elements fareElements = doc.select("form[id^=form_]");

		// maybe the ticket available, but can't be booked online because
		// departure date is near today
		if (fareElements == null || fareElements.isEmpty()) {
			fareElements = doc
					.select("div#itContent table[class=itJadwalText] tr[class=itRowTable0]");

			return parseDirectBooking(fareElements);
		} // ticket can be booked online
		else {
			return parseOnlineBookable(fareElements);
		}
	}

	private List<SearchResult> parseOnlineBookable(Elements fareElements) {
		List<SearchResult> results = Lists.newArrayList();

		for (Element fareElement : fareElements) {
			SearchResult result = new SearchResult();
			result.setTrainName(fareElement.select("input[name=train_name]")
					.first().val());
			result.setTrainNumber(fareElement.select("input[name=train_code]")
					.first().val());

			String departureDate = fareElement
					.select("input[name=origination_date]").first().val();
			String departureTime = fareElement
					.select("input[name=origination_time]").first().val();
			result.setDepartureTime(DATE_TIME_FORMATTER
					.parseDateTime(departureDate + departureTime));

			String arrivalDate = fareElement
					.select("input[name=destination_date]").first().val();
			String arrivalTime = fareElement
					.select("input[name=destination_time]").first().val();
			result.setArrivalTime(DATE_TIME_FORMATTER.parseDateTime(arrivalDate
					+ arrivalTime));

			result.setCabinClass(fareElement.select("input[name=class]")
					.first().val());
			result.setCabinSubClass(fareElement.select("input[name=subclass]")
					.first().val());

			BigDecimal pricePerAdult = new BigDecimal(fareElement
					.select("input[name=ticket_adult]").first().val());
			result.setPricePerAdult(pricePerAdult);

			BigDecimal pricePerChild = new BigDecimal(fareElement
					.select("input[name=ticket_children]").first().val());
			result.setPricePerChild(pricePerChild);

			result.setRemainingTickets(Integer.parseInt(fareElement
					.select("input[name=cek_sisa]").first().val()));
			result.setAvailable(true);
			result.setOnlineBookable(true);

			results.add(result);
		}

		return results;
	}

	private List<SearchResult> parseDirectBooking(Elements fareElements) {
		List<SearchResult> results = Lists.newArrayList();

		for (Element fareElement : fareElements) {
			Matcher trainInfoMatcher = TRAIN_INFO_PATTERN.matcher(fareElement
					.attr("title"));

			if (trainInfoMatcher.find()) {
				Elements infoColumnElements = fareElement.nextElementSibling()
						.select("tr > td[valign=top][align=center]");
				String departureTimeString = infoColumnElements.get(0).text();
				String arrivalTimeString = infoColumnElements.get(1).text();

				Elements priceElements = infoColumnElements
						.select("td > table > tbody > tr[class=itRowTable1]");
				SearchResult result = null;

				for (Element priceElement : priceElements) {
					Elements priceInfoElements = priceElement.select("tr > td");

					// 4 td elements mean it is an single price or adult
					// price
					if (priceInfoElements.size() == 4) {
						result = new SearchResult();
						result.setOnlineBookable(false);
						result.setTrainName(trainInfoMatcher.group(1));
						result.setTrainNumber(trainInfoMatcher.group(2));

						result.setDepartureTime(ALTERNATE_DATE_TIME_FORMATTER
								.parseDateTime(departureTimeString));
						result.setArrivalTime(ALTERNATE_DATE_TIME_FORMATTER
								.parseDateTime(arrivalTimeString));

						Matcher cabinInfoMatcher = CABIN_INFO_PATTERN
								.matcher(priceInfoElements.get(0).ownText());

						if (cabinInfoMatcher.find()) {
							result.setCabinClass(CABIN_NAMES_MAP
									.get(cabinInfoMatcher.group(1)));
							result.setCabinSubClass(cabinInfoMatcher.group(2));
						}

						String pricePerAdultString = StringUtils.remove(
								StringUtils.trim(priceInfoElements.get(2)
										.ownText()), '.');
						BigDecimal pricePerAdult = new BigDecimal(
								pricePerAdultString);
						result.setPricePerAdult(pricePerAdult);
						result.setPricePerChild(pricePerAdult);
						result.setRemainingTickets(Integer
								.parseInt(priceElement.attr("title")));

						String ticketStatus = StringUtils
								.trim(priceInfoElements.get(3).ownText());

						if (StringUtils.equalsIgnoreCase(
								TICKET_AVAILABLE_STATUS, ticketStatus)) {
							result.setAvailable(true);
						} // not available anymore
						else {
							result.setAvailable(false);
						}

						results.add(result);
					} // else means it is a child price
					else {
						if (result != null) {
							String pricePerChildString = StringUtils.remove(
									StringUtils.trim(priceInfoElements.get(2)
											.ownText()), '.');
							BigDecimal pricePerChild = new BigDecimal(
									pricePerChildString);
							result.setPricePerChild(pricePerChild);
						}
					}
				}
			}
		}

		return results;
	}
}
