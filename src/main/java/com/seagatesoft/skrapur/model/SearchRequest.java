package com.seagatesoft.skrapur.model;

import org.joda.time.DateTime;

public class SearchRequest {
	private String id;
	private DateTime departureDate;
	private Station origin;
	private Station destination;
	private int adultPassengers;
	private int childPassengers;
	private int infantPassengers;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public DateTime getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(DateTime departureDate) {
		this.departureDate = departureDate;
	}

	public Station getOrigin() {
		return origin;
	}

	public void setOrigin(Station origin) {
		this.origin = origin;
	}

	public Station getDestination() {
		return destination;
	}

	public void setDestination(Station destination) {
		this.destination = destination;
	}

	public int getAdultPassengers() {
		return adultPassengers;
	}

	public void setAdultPassengers(int adultPassengers) {
		this.adultPassengers = adultPassengers;
	}

	public int getChildPassengers() {
		return childPassengers;
	}

	public void setChildPassengers(int childPassengers) {
		this.childPassengers = childPassengers;
	}

	public int getInfantPassengers() {
		return infantPassengers;
	}

	public void setInfantPassengers(int infantPassengers) {
		this.infantPassengers = infantPassengers;
	}

	public String getAdultPassengersString() {
		return Integer.toString(adultPassengers);
	}

	public String getChildPassengersString() {
		return Integer.toString(childPassengers);
	}

	public String getInfantPassengersString() {
		return Integer.toString(infantPassengers);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('#').append(id).append(',').append(origin.getCode())
				.append('-').append(destination.getCode()).append(',')
				.append(departureDate).append(',').append(adultPassengers)
				.append(',').append(childPassengers).append(',')
				.append(infantPassengers);

		return sb.toString();
	}
}
