package com.seagatesoft.skrapur.model;

import java.math.BigDecimal;

import org.joda.time.DateTime;

/**
 * @author sigitdewanto
 * 
 */
public class SearchResult {
	private String trainName;
	private String trainNumber;
	private DateTime departureTime;
	private DateTime arrivalTime;
	private String cabinClass;
	private String cabinSubClass;
	private BigDecimal pricePerAdult;
	private BigDecimal pricePerChild;
	private boolean available;
	private int remainingTickets;
	private boolean onlineBookable;

	public String getTrainName() {
		return trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public DateTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(DateTime departureTime) {
		this.departureTime = departureTime;
	}

	public DateTime getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(DateTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getCabinSubClass() {
		return cabinSubClass;
	}

	public void setCabinSubClass(String cabinSubClass) {
		this.cabinSubClass = cabinSubClass;
	}

	public BigDecimal getPricePerAdult() {
		return pricePerAdult;
	}

	public void setPricePerAdult(BigDecimal pricePerAdult) {
		this.pricePerAdult = pricePerAdult;
	}

	public BigDecimal getPricePerChild() {
		return pricePerChild;
	}

	public void setPricePerChild(BigDecimal pricePerChild) {
		this.pricePerChild = pricePerChild;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int getRemainingTickets() {
		return remainingTickets;
	}

	public void setRemainingTickets(int remainingTickets) {
		this.remainingTickets = remainingTickets;
	}

	public boolean isOnlineBookable() {
		return onlineBookable;
	}

	public void setOnlineBookable(boolean onlineBookable) {
		this.onlineBookable = onlineBookable;
	}

	@Override
	public String toString() {
		return String
				.format("{\n trainName: %s,\n trainNumber: %s,\n departureTime: %s,\n arrivalTime: %s,\n cabinClass: %s,\n cabinSubClass: %s,\n pricePerAdult: %s,\n pricePerChild: %s,\n available: %s,\n remainingTickets: %s,\n onlineBookable: %s}",
						trainName, trainNumber, departureTime, arrivalTime,
						cabinClass, cabinSubClass, pricePerAdult,
						pricePerChild, available, remainingTickets,
						onlineBookable);
	}
}
