package com.seagatesoft.skrapur.model;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

public class SearchRequestJob {
	private SearchRequest searchRequest;
	private DateTime searchStartTime;
	private DateTime searchEndTime;
	private DateTime postEndTime;
	private int numberOfResults;
	private boolean error;
	private String errorMessage;

	public SearchRequestJob(SearchRequest searchRequest) {
		this.searchRequest = searchRequest;
	}

	public SearchRequest getSearchRequest() {
		return searchRequest;
	}

	public void setSearchRequest(SearchRequest searchRequest) {
		this.searchRequest = searchRequest;
	}

	public DateTime getSearchStartTime() {
		return searchStartTime;
	}

	public void setSearchStartTime(DateTime searchStartTime) {
		this.searchStartTime = searchStartTime;
	}

	public DateTime getSearchEndTime() {
		return searchEndTime;
	}

	public void setSearchEndTime(DateTime searchEndTime) {
		this.searchEndTime = searchEndTime;
	}

	public DateTime getPostEndTime() {
		return postEndTime;
	}

	public void setPostEndTime(DateTime postEndTime) {
		this.postEndTime = postEndTime;
	}

	public int getNumberOfResults() {
		return numberOfResults;
	}

	public void setNumberOfResults(int numberOfResults) {
		this.numberOfResults = numberOfResults;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb
		    .append(searchRequest.getId())
		    .append(',')
		    .append(searchRequest.getOrigin().getCode())
		    .append(',')
		    .append(searchRequest.getDestination().getCode())
		    .append(',')
		    .append(searchRequest.getDepartureDate())
		    .append(',')
		    .append(searchRequest.getAdultPassengers())
		    .append(',')
		    .append(searchRequest.getChildPassengers())
		    .append(',')
		    .append(searchRequest.getInfantPassengers())
		    .append(',');
		
		if (searchStartTime == null) {
			sb.append("not started");
		} else {
			sb.append(searchStartTime);
		}
		
		sb.append(',');
		
		if (searchEndTime == null) {
			sb.append("not finished");
		} else {
			sb.append(searchEndTime);
		}
		
		sb.append(',');

		if (searchStartTime == null || searchEndTime == null) {
			sb.append("");
		} else {
			sb.append(searchEndTime.getMillis() - searchStartTime.getMillis());
		}

		sb.append(',');

		if (postEndTime == null) {
			sb.append("not posting");
		} else {
			sb.append(postEndTime.getMillis() - searchEndTime.getMillis());
		}

		sb
		    .append(',')
		    .append(numberOfResults)
		    .append(',')
		    .append(error)
		    .append(',')
		    .append(StringUtils.isBlank(errorMessage) ? "" : errorMessage);

		return sb.toString();
	}
}
