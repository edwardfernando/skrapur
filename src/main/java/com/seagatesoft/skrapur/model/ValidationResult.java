package com.seagatesoft.skrapur.model;

public class ValidationResult {
	private boolean valid;
	private String message;

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{valid: ").append(valid).append(", message: ")
				.append(message).append('}');

		return sb.toString();
	}
}
