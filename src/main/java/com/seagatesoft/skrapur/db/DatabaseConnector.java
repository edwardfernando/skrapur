package com.seagatesoft.skrapur.db;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import com.seagatesoft.skrapur.Config;
import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;

public class DatabaseConnector {
	private static DatabaseConnector dbConnector;
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final Config config = Config.getInstance();
	private final ComboPooledDataSource cpds;

	private DatabaseConnector() {
		super();

		cpds = new ComboPooledDataSource();

		try {
			cpds.setDriverClass(config.getProperty("jdbc_driver"));
			cpds.setJdbcUrl(config.getProperty("jdbc_url"));
			cpds.setUser(config.getProperty("db_user"));
			cpds.setPassword(config.getProperty("db_password"));
			cpds.setMinPoolSize(Integer.parseInt(config.getProperty("db_min_pool_size")));
			cpds.setMaxPoolSize(Integer.parseInt(config.getProperty("db_max_pool_size")));
		} catch (PropertyVetoException e) {
			logger.error(
			    "Error when setting driver class: {}. Message: {}.",
			    config.getProperty("jdbc_driver"),
			    e.getMessage());
		}
	}

	public static DatabaseConnector getInstance() {
		if (dbConnector == null) {
			dbConnector = new DatabaseConnector();
		}

		return dbConnector;
	}

	public void destroy() {
		try {
			DataSources.destroy(cpds);
		} catch (SQLException e) {
			logger.error("Error when destroying DataSource: {}", e.getMessage());
		}
	}

	public String[] getPopularRoutes() {
		List<String> popularRoutes = Lists.newArrayList();
		String query = "SELECT origin_station_code, destination_station_code FROM popular_routes";

		try {
			Connection conn = cpds.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				String originStationCode = rs.getString("origin_station_code");
				String destinationStationCode = rs.getString("destination_station_code");
				StringBuilder route = new StringBuilder();
				route.append(originStationCode).append('-').append(destinationStationCode);
				popularRoutes.add(route.toString());
			}

			conn.close();
		} catch (SQLException e) {
			logger.error("Error when getting popular routes from database: {}", e.getMessage());
		}

		return popularRoutes.toArray(new String[] {});
	}

	public int expireSearchResults() {
		int affectedRows = 0;
		String query = "UPDATE search_results SET valid_to=NOW() WHERE valid_to IS NULL";

		try {
			Connection conn = cpds.getConnection();
			Statement stmt = conn.createStatement();
			affectedRows = stmt.executeUpdate(query);
			conn.close();
		} catch (SQLException e) {
			logger.error("Error when getting popular routes from database: {}", e.getMessage());
		}

		return affectedRows;
	}

	public void saveSearchResults(SearchRequest searchRequest, List<SearchResult> searchResults) {
		try {
			Connection conn = cpds.getConnection();
			PreparedStatement ps = conn
			    .prepareStatement("INSERT INTO search_results(origin_station_code, origin_station_name, origin_station_city, destination_station_code, destination_station_name, destination_station_city, train_number, train_name, departure_time, arrival_time, cabin_class, cabin_sub_class, price_per_adult, price_per_child, available, remaining_tickets, online_bookable, valid_from) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())");
			ps.setString(1, searchRequest.getOrigin().getCode());
			ps.setString(2, searchRequest.getOrigin().getName());
			ps.setString(3, searchRequest.getOrigin().getCity());
			ps.setString(4, searchRequest.getDestination().getCode());
			ps.setString(5, searchRequest.getDestination().getName());
			ps.setString(6, searchRequest.getDestination().getCity());
			ps.setString(6, searchRequest.getDestination().getCity());

			for (SearchResult searchResult : searchResults) {
				ps.setInt(7, Integer.parseInt(searchResult.getTrainNumber()));
				ps.setString(8, searchResult.getTrainName());
				ps.setTimestamp(9, new java.sql.Timestamp(searchResult
				    .getDepartureTime()
				    .toDate()
				    .getTime()));
				ps.setTimestamp(10, new java.sql.Timestamp(searchResult
				    .getArrivalTime()
				    .toDate()
				    .getTime()));
				ps.setString(11, searchResult.getCabinClass());
				ps.setString(12, searchResult.getCabinSubClass());
				ps.setBigDecimal(13, searchResult.getPricePerAdult());
				ps.setBigDecimal(14, searchResult.getPricePerChild());
				ps.setInt(15, searchResult.isAvailable() ? 1 : 0);
				ps.setInt(16, searchResult.getRemainingTickets());
				ps.setInt(17, searchResult.isOnlineBookable() ? 1 : 0);
				ps.executeUpdate();
			}

			conn.close();
		} catch (SQLException e) {
			logger.error(
			    "Error when saving search results to database: {}. SearchRequest: {}",
			    e.getMessage(),
			    searchRequest);
		}
	}
}
