package com.seagatesoft.skrapur;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Cookie;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;
import com.seagatesoft.skrapur.helper.IndonesianDateHelper;
import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;

public class SearchFormResponseHandler extends AsyncCompletionHandler<Future<List<SearchResult>>> {
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyyMMdd");
	private final Config config = Config.getInstance();
	private final String searchUrl = config.getProperty("search_url");
	private AsyncHttpClient asyncHttpClient;
	private SearchRequest searchRequest;
	private SearchParser searchParser;
	private ProviderNotifier providerNotifier;

	public SearchFormResponseHandler(
	        AsyncHttpClient asyncHttpClient,
	        SearchRequest searchRequest,
	        SearchParser searchParser,
	        ProviderNotifier providerNotifier) {
		this.asyncHttpClient = asyncHttpClient;
		this.searchRequest = searchRequest;
		this.searchParser = searchParser;
		this.providerNotifier = providerNotifier;
	}

	@Override
	public Future<List<SearchResult>> onCompleted(Response searchFormResponse) {
		Future<List<SearchResult>> future = null;

		try {
			Document searchForm = Jsoup.parse(searchFormResponse.getResponseBody());
			String formAction = searchForm.select("form#input").first().attr("action");

			Request searchResultRequest = createSearchResultRequest(
			    formAction,
			    searchFormResponse.getCookies());
			future = asyncHttpClient.prepareRequest(searchResultRequest).execute(
			    new SearchResultResponseHandler(searchRequest, searchParser, providerNotifier));
		} catch (IOException e) {
			providerNotifier.notifySearchingError(searchRequest, "Error when processing search form: " + e.getMessage());
		}

		return future;
	}

	@Override
	public void onThrowable(Throwable t) {
		providerNotifier.notifySearchingError(searchRequest, "Error when requesting search form: " + t.getMessage());
	}

	private Request createSearchResultRequest(String formAction, List<Cookie> cookies) {
		DateTime departureDate = searchRequest.getDepartureDate();
		int monthIndex = departureDate.getMonthOfYear();
		int dayIndex = departureDate.getDayOfWeek();
		String departureDateString = new StringBuilder()
		    .append(IndonesianDateHelper.getDayName(dayIndex))
		    .append(", ")
		    .append(departureDate.getDayOfMonth())
		    .append(" ")
		    .append(IndonesianDateHelper.getMonthName(monthIndex))
		    .append(" ")
		    .append(departureDate.getYear())
		    .toString();

		RequestBuilder searchResultRequestBuilder = new RequestBuilder()
		    .setMethod("POST")
		    .setUrl(searchUrl + formAction)
		    .addHeader("Referer", searchUrl)
		    .addParameter("Submit", "Tampilkan")
		    .addParameter("origination", searchRequest.getOrigin().toString())
		    .addParameter("destination", searchRequest.getDestination().toString())
		    .addParameter(
		        "tanggal",
		        DATE_FORMATTER.print(departureDate) + "#" + departureDateString)
		    .addParameter("adult", searchRequest.getAdultPassengersString())
		    .addParameter("children", searchRequest.getChildPassengersString())
		    .addParameter("infant", searchRequest.getInfantPassengersString());

		for (Cookie cookie : cookies) {
			searchResultRequestBuilder.addCookie(cookie);
		}

		return searchResultRequestBuilder.build();
	}
}
