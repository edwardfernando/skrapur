package com.seagatesoft.skrapur.app;

import junit.framework.TestCase;

public class TestPopularRoutesChecker extends TestCase {
	private PopularRoutesChecker checker;

	@Override
	public void setUp() {
		checker = new PopularRoutesChecker();
		checker.setPopularRoutesFromDb();
	}

	// public void testCheckFaresAsynchronous() {
	// checker.checkFaresAsynchronous();
	// }

	public void testCheckFaresSynchronous() {
		checker.checkFaresSynchronous();
	}
}
