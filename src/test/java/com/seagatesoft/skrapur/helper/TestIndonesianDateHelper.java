package com.seagatesoft.skrapur.helper;

import junit.framework.TestCase;

public class TestIndonesianDateHelper extends TestCase {

	public void testGetMonthName() {
		assertEquals("Januari", IndonesianDateHelper.getMonthName(1));
		assertEquals("Februari", IndonesianDateHelper.getMonthName(2));
		assertEquals("Maret", IndonesianDateHelper.getMonthName(3));
		assertEquals("April", IndonesianDateHelper.getMonthName(4));
		assertEquals("Mei", IndonesianDateHelper.getMonthName(5));
		assertEquals("Juni", IndonesianDateHelper.getMonthName(6));
		assertEquals("Juli", IndonesianDateHelper.getMonthName(7));
		assertEquals("Agustus", IndonesianDateHelper.getMonthName(8));
		assertEquals("September", IndonesianDateHelper.getMonthName(9));
		assertEquals("Oktober", IndonesianDateHelper.getMonthName(10));
		assertEquals("November", IndonesianDateHelper.getMonthName(11));
		assertEquals("Desember", IndonesianDateHelper.getMonthName(12));
		assertNull(IndonesianDateHelper.getMonthName(-1));
		assertNull(IndonesianDateHelper.getMonthName(0));
		assertNull(IndonesianDateHelper.getMonthName(13));
	}
	
	public void testGetDayName() {
		assertEquals("Senin", IndonesianDateHelper.getDayName(1));
		assertEquals("Selasa", IndonesianDateHelper.getDayName(2));
		assertEquals("Rabu", IndonesianDateHelper.getDayName(3));
		assertEquals("Kamis", IndonesianDateHelper.getDayName(4));
		assertEquals("Jumat", IndonesianDateHelper.getDayName(5));
		assertEquals("Sabtu", IndonesianDateHelper.getDayName(6));
		assertEquals("Minggu", IndonesianDateHelper.getDayName(7));
		assertNull(IndonesianDateHelper.getDayName(-1));
		assertNull(IndonesianDateHelper.getDayName(0));
		assertNull(IndonesianDateHelper.getDayName(8));
	}
}
