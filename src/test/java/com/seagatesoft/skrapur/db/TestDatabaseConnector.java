package com.seagatesoft.skrapur.db;

import java.util.List;

import junit.framework.TestCase;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seagatesoft.skrapur.SearchProvider;
import com.seagatesoft.skrapur.model.SearchRequest;
import com.seagatesoft.skrapur.model.SearchResult;
import com.seagatesoft.skrapur.model.Station;

public class TestDatabaseConnector extends TestCase {
	private final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyyMMdd");
	private final Logger consoleLogger = LoggerFactory.getLogger("console");

	public void testGetPopularRoutes() {
		DatabaseConnector dbConnector = DatabaseConnector.getInstance();
		String[] popularRoutes = dbConnector.getPopularRoutes();
		dbConnector.destroy();

		assertNotSame(0, popularRoutes.length);

		if (consoleLogger.isInfoEnabled()) {
			for (String route : popularRoutes) {
				consoleLogger.info(route);
			}
		}
	}

	public void testSaveSearchResults() {
		SearchRequest searchRequest = createSearchRequest();
		SearchProvider provider = new SearchProvider();

		try {
			List<SearchResult> searchResults = provider.search(searchRequest);
			assertFalse(searchResults.isEmpty());
			
			if (consoleLogger.isInfoEnabled()) {
				consoleLogger.info("Found {} results.", searchResults.size());
			}
			
			DatabaseConnector dbConnector = DatabaseConnector.getInstance();
			dbConnector.saveSearchResults(searchRequest, searchResults);
			dbConnector.destroy();

		} catch (Exception e) {
			consoleLogger.error("Error when scraping: {}", e.getMessage());
			e.printStackTrace();
		}
	}

	public void testExpireSearchResults() {
		DatabaseConnector dbConnector = DatabaseConnector.getInstance();
		assertNotSame(0, dbConnector.expireSearchResults());
		dbConnector.destroy();
	}

	private SearchRequest createSearchRequest() {
		SearchRequest searchRequest = new SearchRequest();
		Station origin = new Station();
		origin.setCode("PSE");
		searchRequest.setOrigin(origin);
		Station destination = new Station();
		destination.setCode("YK");
		searchRequest.setDestination(destination);
		searchRequest.setDepartureDate(DATE_FORMATTER.parseDateTime("20131108"));
		searchRequest.setAdultPassengers(1);
		searchRequest.setChildPassengers(1);
		searchRequest.setInfantPassengers(0);

		return searchRequest;
	}
}
