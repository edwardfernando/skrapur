# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.27)
# Database: skrapur
# Generation Time: 2013-10-29 13:21:53 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table popular_routes
# ------------------------------------------------------------

CREATE TABLE `popular_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_station_code` varchar(5) NOT NULL,
  `destination_station_code` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table search_results
# ------------------------------------------------------------

CREATE TABLE `search_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origin_station_code` varchar(5) NOT NULL,
  `origin_station_name` varchar(255) NOT NULL,
  `origin_station_city` varchar(255) NOT NULL,
  `destination_station_code` varchar(5) NOT NULL,
  `destination_station_name` varchar(255) NOT NULL,
  `destination_station_city` varchar(255) NOT NULL,
  `train_number` int(11) NOT NULL,
  `train_name` varchar(255) NOT NULL,
  `departure_time` datetime NOT NULL,
  `arrival_time` datetime NOT NULL,
  `cabin_class` char(1) NOT NULL,
  `cabin_sub_class` char(1) NOT NULL,
  `price_per_adult` decimal(18,2) NOT NULL,
  `price_per_child` decimal(18,2) NOT NULL,
  `available` tinyint(4) NOT NULL,
  `remaining_tickets` int(11) NOT NULL,
  `online_bookable` tinyint(4) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
